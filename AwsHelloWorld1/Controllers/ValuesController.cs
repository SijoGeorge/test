using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AwsHelloWorld1.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
		// GET api/values
	    [HttpGet]
	    public IEnumerable<string> Get()
	    {
		    try
		    {
			    using (var context = new CsContractRepositoryContext())
			    {
				    var books = context.test;
				    foreach (var book in books)
				    {


				        return new string[] { "Database3", "Connected" };

                        //Console.WriteLine(book.Age.ToString());
				    }
			    }
		    }
		    catch (Exception e)
		    {
		        return new string[] { "Error", e.ToString() };
		    }
		    // Gets and prints all books in database
		    return new string[] { "Hello", "World" };
	    }

		// GET api/values/5
		[HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
