﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AwsHelloWorld1
{
	public class test
	{
		[Key]
		public int Age { get; set; }
	}
	public class CsContractRepositoryContext : DbContext
	{
	    public DbSet<test> test { get; set; }
	    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	    {
            //optionsBuilder.UseMySQL("server=localhost;database=CSContractRepository;user=sysramas;password=Ramas_01");
            //optionsBuilder.UseMySQL("server=localhost;port=3306;database=CSContractRepository;uid=sysramas;password=Ramas_01");
	        optionsBuilder.UseMySQL("server=cscontractrepositoryinstance.c8kwpxgh6onl.eu-west-1.rds.amazonaws.com;port=3306;database=CSContractRepository;uid=sysramas;password=Ramas_01");
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
	    {
		    base.OnModelCreating(modelBuilder);

		    //modelBuilder.Entity<Publisher>(entity =>
		    //{
		    //    entity.HasKey(e => e.ID);
		    //    entity.Property(e => e.Name).IsRequired();
		    //});

		    //modelBuilder.Entity<Book>(entity =>
		    //{
		    //    entity.HasKey(e => e.ISBN);
		    //    entity.Property(e => e.Title).IsRequired();
		    //    entity.HasOne(d => d.Publisher)
		    //        .WithMany(p => p.Books);
		    //});
	    }
	}
}
